//
//  MapView.swift
//  ProjectPhase3
//
//  Created by Kevan Patel on 12/6/18.
//  Copyright © 2018 BitShifters. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapView: UIViewController {
    
    /// MKMapView of view controller in storyboard
    @IBOutlet weak var mapView: MKMapView!
    
    /// location manager for current location
    let manager = CLLocationManager()
    /// how far map is zoomed out
    let regionMeters: Double = 3000
    
    /// Sets up Location Manager to get current location and checks if application has permission to access current location
    override func viewDidLoad() {
        super.viewDidLoad()
        checkServices()
    }
    
    /// Checks if application is using up too much memory, can crash application
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// Sets location manager delegate and makes current location accuracy the best possible one
    func setupManager() {
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    
    /// Sets center of map as current location
    func centerLocation() {
        if let location = manager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionMeters, longitudinalMeters: regionMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    
    /// Gets coordinates of center of map, which is the marker
    ///
    /// - Parameter mapView: MKMapView variable for embedded map
    /// - Returns: center location coordinates
    func getCenter(for mapView: MKMapView) -> CLLocation {
        let lat = mapView.centerCoordinate.latitude
        let lon = mapView.centerCoordinate.longitude
        
        return CLLocation(latitude: lat, longitude: lon)
    }
    
    /// Checks if location services are enabled, if they are sets up location manager and checks permissions
    func checkServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupManager()
            checkAuthorization()
        }
        else {
            print("CHECK PERMISSIONS")
        }
    }
    
    /// Tracks and updates user's current location
    func trackLocation() {
        mapView.showsUserLocation = true
        centerLocation()
        manager.startUpdatingLocation()
    }
    
    /// Checks permissions given by user, if a user doesn't have permissions the current location function of the map will not work
    func checkAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            trackLocation()
            break
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .denied:
            break
        case .authorizedAlways:
            break
        case .restricted:
            break
        }
    }
    
}

// MARK: - CLLocationManagerDelegate extension of MapView
extension MapView: CLLocationManagerDelegate {
    
    /// Sets region of map (what the user currently sees) zoomed in on current location
    ///
    /// - Parameters:
    ///   - manager: location manager
    ///   - locations: current location coordinates
    func manager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.last else {
            print("CURRENTLOCATION GUARD")
            return
        }
        
        let centerCoordinate = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        let regionCoordinate = MKCoordinateRegion.init(center: centerCoordinate, latitudinalMeters: regionMeters, longitudinalMeters: regionMeters)
        mapView.setRegion(regionCoordinate, animated: true)
    }
    
    /// Checks if user has given app permission to access current location so location manager can grab it
    ///
    /// - Parameters:
    ///   - manager: location manager
    ///   - status: authorization status of current location
    func manager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAuthorization()
    }
}


