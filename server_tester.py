from locust import HttpLocust, TaskSet
import time

VEHICLE_DATA = {
    'date': time.now(),
    'speed': 1,
    'gas': 1,
    'longitude': 0,
    'latitude': 0,
}

VEHICLE = {
    'uid': 0,
    'mrLat': 0,
    'mrLong': 0,
    'mrSpeed': 0,
    'mrGas': 0,
    'data': [
        VEHICLE_DATA,
        VEHICLE_DATA,
    ],
}


def create(l):
    l.client.post('/vehicle', {"username": "ellen_key", "password": "education"})


def read(l):
    l.client.get('/vehicle/1')


def update(l):
    l.client.put('/vehicle/1', VEHICLE)


def delete(l):
    l.client.delete('/vehicle', VEHICLE)


def list_all(l):
    l.client.get('/vehicle')


class VehicleBehavior(TaskSet):
    tasks = {create: 2, read: 1, update: 1, delete: 1, list_all: 1}

    def on_start(self):
        create(self)
        read(self)
        update(self)
        delete(self)
        list_all(self)


class VehicleUser(HttpLocust):
    task_set = VehicleBehavior
    min_wait = 5000
    max_wait = 9000
