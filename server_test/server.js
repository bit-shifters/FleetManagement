var express = require('express'),
	app = express(),
	ports = process.env.PORT || 8080,
	mongoose = require('mongoose'),
	Example = require('./api/models/exampleModel'),
	Manager = require('./api/models/managerModel'),
	Vehicle = require('./api/models/vehicleModel'),
	bodyParser = require('body-parser'),
	nconf = require('nconf'),
	cluster = require('cluster'),
	numCPUs = require('os').cpus().length,
	uri = 'mongodb://fleet:manager@ds259855.mlab.com:59855/fleetdb',
	urilocal = 'mongodb://localhost/exampledb';

mongoose.Promise = global.Promise;

if (cluster.isMaster) {
	console.log(`Master ${process.pid} is running`);

	// Fork workers.
	for (let i = 0; i < numCPUs; i++) {
		cluster.fork();
	}

	cluster.on('exit', (worker, code, signal) => {
		console.log(`worker ${worker.process.pid} died`);
	});
} else {
	console.log(`Worker ${process.pid} started`);

	var server = false;
	if (server) {
		nconf.argv().env().file('keys.json');
		mongoose.connect(uri, { useMongoClient: true });
	} else {
		mongoose.connect(urilocal, { useMongoClient: true });
	}

	app.use(bodyParser.urlencoded({extended: true }));
	app.use(bodyParser.json());

	var routes = require('./api/routes/exampleRoute');
	routes(app);

	app.listen(ports);
}
