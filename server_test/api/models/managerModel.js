var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Manager Schema
var ManagerSchema = new Schema({
	username: {
		type: String,
		index:true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	vehicles: [{
		uid: Number,
		required: true
	}]
});

module.exports = mongoose.model('Managers', ManagerSchema);

