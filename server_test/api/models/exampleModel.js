var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dataSchema = new Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	speed: {
		type: Number,
		required: true
	},
	gas: {
		type: Number,
		required: true
	},
	longitude: {
		type: Number,
		min: -180,
		max: 180,
		required: true
	},
	latitude: {
		type: Number,
		min: -90,
		max: 90,
		required: true
	}
});
var ExampleSchema = new Schema({
	uid: {
		type: Number,
		index: true,
		required: true
	},
	data: [dataSchema]

});

module.exports = mongoose.model('Examples', ExampleSchema);
