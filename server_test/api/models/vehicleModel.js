var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dataSchema = new Schema({
	date: {
		type: Date,
		default: Date.now,
		required: true
	},
	speed: {
		type: Number,
		required: true
	},
	gas: {
		type: Number,
		required: true
	},
	longitude: {
		type: Number,
		min: -180,
		max: 180,
		required: true
	},
	latitude: {
		type: Number,
		min: -90,
		max: 90,
		required: true
	}
});
var VehicleSchema = new Schema({
	uid: {
		type: Number,
		index: true,
		required: true
	},
	mrLat: {
		type: Number,
		min: -90,
		max: 90,
		required: true
	},
	mrLong: {
		type: Number,
		min: -180,
		max: 180,
		required: true
	},
	mrSpeed: {
		type: Number,
		required: true
	},
	mrGas: {
		type: Number,
		required: true
	},
	data: [dataSchema]
});

module.exports = mongoose.model('Vehicles', VehicleSchema);
