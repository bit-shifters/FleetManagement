from locust import HttpLocust, TaskSet, task
import random 

class APIVehicleDataTasks(TaskSet):
    def on_start(self):
        self.client.post("/vehicle")

    @task
    def update_vehicle(self):
        self.client.put("/vehicle/pid/" + random.randint(1, 1000),
        {
            "gas": random.randint(1,100)
            "speed": random.randint(1,100)
            "latitude": random.randint(1, 1000) * 5,
            "longitude": random.randit(1, 1000) * 5
        })

class APIVehicleDataUser(HttpLocust):
    task_set = APIVehicleTasks
    min_waut = 5000
    max_wait = 15000
    
        